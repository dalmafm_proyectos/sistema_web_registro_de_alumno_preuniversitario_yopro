import axios from 'axios'

export default{
    namespaced:true,
    state:{
        comments:[]
     
    },
    getters:{
       
    },
    mutations:{
      
    },
    actions:{
        async createComment({dispatch},comment){
            console.log('solicitando la creacion de un nuevo comentario')
            console.log(comment)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/comments',comment)
        },

        meComment({commit},data){
          
        }
    }
}
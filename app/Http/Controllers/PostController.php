<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon; 
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Carbon::setlocale('es');
        $posts = Post::latest()->paginate(17);
        foreach($posts as $post){
            $post ->setAttribute('user',$post->user);
            $post ->setAttribute('added',carbon::parse($post->created_at)->diffForHumans());
            $post ->setAttribute('path',$post->slug);
        }
        return response()->json($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        if($request['file']){
            $imageName = time().'.'.$request['file']->getClientOriginalExtension();
        
            //dd($imageName);
            //dd('felicidad llego aqui');
            $data = json_decode($request['form']);
            //dd($data['body']);
            
            Post::create([
                'title'=>$data->title,
                'slug' =>Str::slug($data->title),
                'ci'=> $data ->ci,
                'edad'=> $data ->edad,
                'carrera'=> $data ->carrera,
                'body'=> $data ->body,
                'category_id' => $data->category,
                'user_id' => Auth::user()->id,
                'photo' => '/uploads/' .$imageName
                //'photo' => 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.freepik.es%2Fvector-gratis%2Fconcepto-universidad-dibujado-mano_4580941.htm&psig=AOvVaw29lH-70ufUr1Ay4w96zrcT&ust=1613484482848000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJiMrqOI7O4CFQAAAAAdAAAAABAW'
            ]);
            $request['file']->move(public_path('uploads'),$imageName);
            return response()->json (['state'=>'existo de creacion con publicacio'], 200);

        }else{
            return response()->json (['state'=>'no existe imagen para la publicacion'], 404);

        }
        
    } 
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //dd('respondiendo la solicitud del detalle de la publicacion');
        Carbon::setlocale('es');
        return response()->json([
            'id'=>$post->id,
            'title'=>$post->title,
            'body'=>$post->body,
            'ci'=>$post->ci,
            'edad'=>$post->edad,
            'carrera'=>$post->carrera,
            'photo'=>$post->photo,
            'created_at'=>$post->created_at->diffForHumans(),
            'user'=>$post->user->name,
            'category'=>$post->category->name,
            'categoryId'=>$post->category->id,
            'comments_count'=>$post->comments->count(),
            'comments'=>$this->allCommentsPost($post->comments)

        ]);
    }

public function allCommentsPost($comments){
    $jsonComments=[];
    foreach($comments as $comment){
        array_push($jsonComments,[
            'id'=>$comment->id,
            'body'=>$comment->body,
            'created'=>$comment->created_at->diffForHumans(),
            'user'=>$comment->user->name,

        ]);
    }
    return $jsonComments;
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //dd('este es el metodo para actualizar por el model por defecto');
        //dd($request->all());
        //dd(Auth::user()->id .'==='. $post->user_id);
        if(Auth::user()->id == $post->user_id){
            if($request['file']){
                $imageName = time().'.'.$request['file']->getClientOriginalExtension();
                $request['file']->move(public_path('uploads'),$imageName);
                $post->photo = '/uploads/'.$imageName;
                //dd('existe imagen');
            }

            $form = json_decode($request['form']);
        //dd($form);
            $post->title = $form->title;
            $post->ci = $form->ci;
            $post->edad = $form->edad;
            $post->carrera = $form->carrera;
            $post->slug = Str::slug($form->title);
            $post->category_id = $form->category;
            $post->body = $form->body;
        
        $post->save();
        return response()->json (['state'=>'Editand con existo'], 200);
        
        }else{
            return response()->json (['state'=>'LA PUBLICACION NO LE PERTENECE'], 401);

        }
    
    }
    public function updateMyPost(Request $request){
        //dd(`aui tambien voy a actualizar`);
        //dd($request->all());
        //dd('este es su propio metodo');
        //Post::where('id '==)
        $form = json_decode($request['form']);
        $post = Post::where('id',$form->idpost)->first();
        //dd($post);
        if(Auth::user()->id == $post->user_id){
            if($request['file']){
                $imageName = time().'.'.$request['file']->getClientOriginalExtension();
                $request['file']->move(public_path('uploads'),$imageName);
                $post->photo = '/uploads/'.$imageName;
                //dd('existe imagen');
        }else{

            $form = json_decode($request['form']);
        //dd($form);
            $post->title = $form->title;
            $post->slug = Str::slug($form->title);
            $post->category_id = $form->category;
            $post->body = $form->body;
        }
        $post->save();
        return response()->json (['state'=>'Editand con existo por su propio metodo'], 200);
        }else{
            return response()->json (['state'=>'LA PUBLICACION NO LE PERTENECE'], 401);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(Auth::user()->id == $post->user_id){
            $post->delete();
            return response()->json (['state'=>'la publicacion ha sido eleiminado con exito '], 200);
        }else{
            return response()->json (['state'=>'no le pertenece la publicacion'], 401);
        }
        //dd('Aqui eliminaremos la publicacion');
        
       
    }
    public function allPostForUser(){
        Carbon::setlocale('es');
        $posts = Post::latest()->where('user_id',Auth::user()->id)->paginate(8);
        foreach($posts as $post){
            $post ->setAttribute('user',$post->user);
            $post ->setAttribute('added',carbon::parse($post->created_at)->diffForHumans());
            $post ->setAttribute('path','/post/'.$post->slug);
        }
        return response()->json($posts);

    }
}
